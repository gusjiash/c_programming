

#include <stdio.h>
#include <stdlib.h>

//#define errno (*__errno_location ()) 


int main() 
{ 
      FILE* fil; 
      char filnamn[]= {"fil1.txt"};
      char namn[]="Peter";   
      // For getting an error erase the file fil1.txt and change mode to "r" , compile and run.
      if((fil = fopen(filnamn, "w")) == NULL){  
          printf("\n Kunde inte �ppna %s \n Felmeddelande: %s\n", filnamn, strerror(errno)); 
          system("PAUSE");
          return 1; 
      } 

      fprintf(fil, "Skapades av %s\n", namn); 
      fclose(fil); 
      system("PAUSE");
      return 0; 
} 
