
/********************************************
      Demoprog  C-programming              **
      Kurs DIT165 Embedded                 **
      Fil: linkedlist2_emb.c               **
      Demoexempel listhantering            **
      2013-02-12  / Peter Lundin           **
********************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//#### Constants #####
#define MAX 20

// ##### Type declaration ####

typedef struct q{
        int data;
        struct q *prev; // previus struct
        struct q *next; // next struct
}POSTTYP ;


typedef struct  {
        POSTTYP *head;
        POSTTYP *current;
}LISTTYP;


// ##### Function declarations #####

LISTTYP* newque();
void add_new_element_last( LISTTYP *Quetop );
void delete_list(LISTTYP *quetop);
void clear_list(LISTTYP *quetop);  // Not implemented in this file

//###### Main program #######
int main(int argc, char *argv[])
{
    LISTTYP *que1, *que2;
    int nr=0;
    printf("Time for start . \n");
    que1=newque();
    que2=newque();

    printf("\n Data is %d", que2->head->data );
    // L?g till 10 element till listan.
    for ( nr=0; nr<10; nr++){
        add_new_element_last( que1);
        add_new_element_last( que2);
    }
    // Set curren pointer pointing to record nr 1
    que1->current=que1->head;
    que2->current=que2->head;

     // Print out all records of the list.
     while(!(que2->current->next==NULL)){
            printf("\n Data is %d", que1->current->data );
            que1->current=que1->current->next;
            printf("\n Data is %d", que2->current->data );
            que2->current=que2->current->next;

     }
     printf("\n Data is %d", que2->current->data );
  // ------- Free all memory ----------
  delete_list(que1);
  delete_list(que2);

  //------------------
  system("PAUSE");
  return 0;
}

LISTTYP* newque(){
    int i=0;
    LISTTYP *quetemp;
    quetemp=(LISTTYP*) malloc(sizeof(LISTTYP));
    //pointer always points to the first element of struct

    quetemp->head=(POSTTYP*) malloc(sizeof(POSTTYP));
    quetemp->current=quetemp->head;
    quetemp->head->next=NULL;
    quetemp->head->prev=NULL;
    quetemp->head->data=1;

    return(quetemp);
}

void add_new_element_last( LISTTYP *quetop ){
     static int nr=2;
     POSTTYP *newelement;
     // Set curren pointer pointing to record nr 1
     quetop->current=quetop->head;
     // Find last record
     while( !(quetop->current->next==NULL)){
            quetop->current=quetop->current->next;
     }
     // Create a new record
     newelement=(POSTTYP*) malloc(sizeof(POSTTYP));
     quetop->current->next=newelement;
     newelement->next=NULL;
     newelement->prev=quetop->current;
     newelement->data=nr;
     nr++;
}

void delete_list(LISTTYP *quetop) {
     POSTTYP *tempelement;
    // Set curren pointer pointing to record nr 1
     quetop->current=quetop->head;
     // Find last record.
     while( !(quetop->current->next==NULL)){
            quetop->current=quetop->current->next;
     }


    while( !( quetop->current==NULL  )){

           tempelement=quetop->current;
           quetop->current=tempelement->prev;
           free(tempelement);
    }
           free(quetop);

}
