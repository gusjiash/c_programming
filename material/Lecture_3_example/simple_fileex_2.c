/*---------------------------------------*
*   Demo ex : a simple file register    **
*   File simple_fileex_1.c              **
*   Peter Lundin 2013-02-12             **
*----------------------------------------*/

#include <stdio.h>
#include <curses.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#define MAX 10

typedef struct {
  char lname[20];
  char fnamn[20];
  } POSTTYPE;

//--- Function declarations ----------
int  enter_rec(POSTTYPE reg[],int actual_number);
void store( POSTTYPE reg[],int num );
int readfile(POSTTYPE reg[]);

int    numal=0;
//----------- Main program --------------

int main()
{
   int    number_of=0;
   POSTTYPE card[MAX];
   printf("Time for start\n" );
   number_of=enter_rec(card,number_of);
   printf(" Number of records is %d", number_of);
   printf("\n");
   store(card,number_of);
   puts("\n Now it is stored");
   //  getch();
   number_of=readfile(card);
	 printf("\n Number of records in file is  %d . ",number_of);
   puts(card[2].lname);
   getch();
   return 0;
}

int enter_rec(POSTTYPE reg[],int actual_number) {
   int i,l,k;
   char temp[20];
   for( i=actual_number; i<MAX; i++){
     printf("\n Enter first name ( Just a breakes ) :  ");
     gets(temp);
     if(strlen(temp)==1 && temp[0]=='a') break;
     strcpy(reg[i].fnamn,temp);
     printf("\n Enter last name  :  ");
     gets(temp);
     strcpy(reg[i].lname,temp);
     //gets(temp);// Clears eventual rest characters in buffer
  }
   return(i);
}

void store(POSTTYPE *regpek,int num ) {
   FILE *filpek;
   int n;
   if((filpek=fopen("creg1.dat","wb"))==NULL) {
     printf("\n Can not create the file");
     exit(1);
   }
   for(n=0;n<num;n++){
       fwrite(regpek, sizeof(POSTTYPE),1,filpek);
			 regpek++;
   }

   fclose(filpek);
}
int readfile(POSTTYPE *regpek){
   int n=0;
   FILE *filpek;
   if((filpek=fopen("creg1.dat","rb"))==NULL) {
     printf("\n Can not open file");
     exit(1);
   }
   // --------   Reads all file  --------
   while(!feof(filpek)){
          if(fread(regpek,sizeof(POSTTYPE),1,filpek)){
             printf("\n %s",regpek->lname);
             n++;
             regpek++;
          }
   }
   fclose(filpek);
   system("PAUSE");
   return n;
}
