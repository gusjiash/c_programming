
/********************************************
      Demoprog  C-programming              **
      Kurs DIT165 Embedded                 **
      Fil: linkedlist1_emb.c               **
      Demo double linked list              **
      2013-02-12  / Peter Lundin           **
****************************************** **/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

//#### Constants #####
#define MAX 20

// ##### Typdeklaretioner ####
typedef struct q{
        int data;
        struct q *prev;
        struct q *next;
} REGTYPE;

// ##### Declaration of functions #####

REGTYPE* random_list();

//###### Main program #######
int main(int argc, char *argv[])
{
    int nr=0;
    REGTYPE *akt_post, *temp, *head=NULL;
    srand(time(0));
    printf("Time for running. \n");
    head=random_list();
    akt_post=head;
    while( akt_post!=NULL){
           printf("\n Post nr %d : %d" , nr++, akt_post->data);
           akt_post=akt_post->next;
    }
  // --- Free all memory -----

  while((akt_post=head)!=NULL){
     head=akt_post->next;
     free(akt_post);
  }

  system("PAUSE");
  return 0;
}

REGTYPE* random_list(){
    int nr, i=0;
    REGTYPE *top, *old, *item;
    top=(REGTYPE*) malloc(sizeof(REGTYPE));
    nr=rand()%100;
    top->data=nr;
    top->prev=NULL;
    top->next=NULL;
    item=top;
    i++;
    while(i<MAX){
                 old=item;
                 item=(REGTYPE*) malloc(sizeof(REGTYPE));
                 nr=rand()%100;
                 item->data=nr;
                 item->prev=old;
                 item->next=NULL;
                 old->next=item;
                 i++;
    }
    return(top);
}

