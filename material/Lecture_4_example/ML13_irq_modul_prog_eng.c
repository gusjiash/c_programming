

// 	Example for demo use of moduls C and assembly
//      File ML13_irq_modul_prog_eng.c


#define SENSOR  1
// Register addresses
#define ML13_Status	0x0B00
#define ML13_Control	0x0B00
#define ML13_IRQ_Control 0x0B01
#define ML13_IRQ_Status 0x0B01

// Macros for read and write in ML13 register.

#define REG8(x) *((unsigned char *)(x))

// Macro for write IRQ routin adress in vector table.
#define  SET_IRQ_VECTOR(interrupt_handler, address)    *((unsigned int *) address )= &(interrupt_handler)

// ------    Declaration of functions in ML13_irq_asm_routin.s12
extern void init_irq(void);	
extern void standby(void);

// ------    Declariation of the interrupt routin
__interrupt void  ML13_interrupt( void );

// A global variable affected by the interrupt handler void  ML13_interrupt( void ) 
// and by the function timeout in the asm-file. 

int	interrupt_type;

// ---------- Main program -------
void	main()
{

  int nr=0;
  
   // set up  interrupt handler for ML13 irq...
  SET_IRQ_VECTOR(ML13_interrupt, 0x3FF2);  // Adress for IRQ routin to the exception vector table
  
  init_irq();                     // Call for the _init_irg routin in the assembly file           
  
  while(1){
    
    // Standby is defind in the asm-file and program stops here and wait for a IRQ call.
	// The IRQ routin will be executed ant the program continues after that.
    standby();
    
     switch(interrupt_type){
	
	
	
    }
  }
}



// Interrupt service function for hardware irq ( vector address 0x3FF2 )
// This routin will be called when any IRQ from the ML13 modul( Sensor(Key stroke) , door opened, door closed..)
__interrupt void  ML13_interrupt( void )

{
     puts("\n Avbrott");
    
    if( REG8(ML13_IRQ_Control) & 0x0C){
        puts("\n Avbrott_inne");
	interrupt_type = SENSOR;
	
     }
     REG8(ML13_IRQ_Control)=0x01;   // Ack of IRQ
     
}


     
