
/*
  	Event driven "IRQ demo" ML13
	// File ML13_irq_demo_eng.c
	// No other files.
*/

#include <stdio.h>
// Following TYPES is defined and could be used to specify type of interrupt with 
// the variable int interrupttype. ( Not : The name is convered to _interrupttype by the compiler)

#define ML13_IRQ_Control 0x0B01
#define ML13_IRQ_Status  0x0B01

// defines for the 8 bit Digital outport
#define PORTA      0x0400 // IN-port 
#define PORTB      0x0402 // UT-port

// Macros for read and write in ML13 register.
#define REG8(x) *((unsigned char *)(x))


// Macro for write IRQ routin adress in vector table.
#define  SET_IRQ_VECTOR(interrupt_handler, address)    *((unsigned int *) address )= &(interrupt_handler)


// ------    Declaration of functions 
void initIrq(void);
	
__interrupt void  ML13_interrupt( void );


// A global variable affected by the interrupt handler void  ML13_interrupt( void ) 
// and by the function timeout in the asm-file. 

int	interrupt_type;

// ---------- Main program -------
void	main()
{
  int n;
  unsigned char  dig=0x0;
  initIrq();                 // Call for the _init_irg routin in the assembly file           
  
  while(1){
   // do anything 
   
   for ( n=0; n<300; n++);  
   dig=dig+1;
   REG8(PORTB)=dig;
  }
}


// ---------------initiation for IRQ handle of ML13 irq
void initIrq(void){

  SET_IRQ_VECTOR(ML13_interrupt, 0x3FF2);   // Adress for IRQ routin to the exception vector table

  __asm(" CLI");                    // Inline assemby  , Clear IRQ mask bit in CPU CCR register
  
  REG8(ML13_IRQ_Control)= 0x00;     // Enable IRQ from ML13 unit
}
  

// Interrupt service function for hardware irq ( vector address 0x3FF2 )
// This routin will be called when any IRQ from the ML13 modul occurs ( Sensor(Key stroke)
__interrupt void  ML13_interrupt( void )

{
     static int nr = 0;            // Keeps value between calls
     puts("\n Avbrott igen     ");
     nr++;
     putchar(nr+48);
     REG8(ML13_IRQ_Control)= 0x01; //   acknowledge of IRQ
}


     
