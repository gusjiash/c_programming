
// Program absadr_2.c   Bithantering 

#define REG8(x) *((unsigned char *)(x))
#define BIT7       0x80
#define PORTA      0x0400 // IN-port 
#define PORTB      0x0402 // UT-port

int main(void){
  unsigned char instatus;
  while(1){
    instatus=REG8(PORTA);
    if (REG8(PORTA)& BIT7) {
         //REG8(PORTB)=(REG8(PORTB)) | 0x10 ;
         REG8(PORTB)=instatus+128;
    } else {
         REG8(PORTB)=instatus;
    }
  }
  return(0);
}
