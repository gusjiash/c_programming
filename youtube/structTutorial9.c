
// How to enter information to linked list
// How to pull information out from linked list


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct product{
    float price;
    char productName[30];
    struct product *next;
};

struct product *pFirstNode = NULL;
struct product *pLastNode = NULL;

void creatNewList(){

    struct product *pNewStruct = (struct product *) malloc(sizeof(struct product));

    pNewStruct->next = NULL;

    printf("Enter Product Name\n");
    scanf("%s", &(pNewStruct)->productName);

    printf("Enter Product Price\n");
    scanf(" %f", &(pNewStruct)->price);

    pFirstNode = pLastNode = pNewStruct;
}


void inputData(){
    if (pFirstNode == NULL){ //know we are working on the second node

        creatNewList();

        }else{

            struct product *pNewStruct = (struct product *) malloc(sizeof(struct product));

            printf("Enter Product Name\n");
            scanf("%s", &(pNewStruct)->productName);

            printf("Enter Product Price\n");
            scanf(" %f", &(pNewStruct)->price);

            if (pFirstNode == pLastNode){

                pFirstNode->next = pNewStruct;

                pLastNode = pNewStruct;

                pNewStruct->next = NULL;

            }else{ // add to the third and all the other node after it
                pLastNode->next = pNewStruct;

                pNewStruct->next = NULL;

                pLastNode = pNewStruct;
            }
        }

}

void outputData(){
    struct  product *pProducts = pFirstNode;

    printf("Products Entered\n\n");

    while(pProducts != NULL){
        printf("%s costs %.2f\n\n", pProducts->productName,pProducts->price);

        pProducts = pProducts->next;
    }

}

struct product* searchForProduct(char *productName){
    struct product* pProductIterator = pFirstNode;

    while(pProductIterator != NULL){
        int areTheyEqual = strncmp(pProductIterator->productName, productName, 30);

        if(!areTheyEqual){
            printf("%s was found and it costs %.2f \n\n",
                pProductIterator->productName,
                pProductIterator->price);

            return pProductIterator;
        }

        pProductIterator = pProductIterator->next;

    }
    printf("%s Wasnt found \n", productName);

    return NULL;
}


void removeProduct(char * productName){

    struct product *pProductToDelete = NULL;

    pProductToDelete = searchForProduct(productName);

    if(pProductToDelete != NULL){

        printf("%s Was Deleted\n\n", productName);

         // If we want to delete the first product we
         // must assign the next product in the list
         // to the firstNode

        if(pProductToDelete == pFirstNode){

            pFirstNode = pProductToDelete->next;

        } else {

            // Get the product to delete and assign its next
             // to the product before the product that is deleted

            pProductBeforeProductToDelete->next = pProductToDelete->next;

         }

        free(pProductToDelete);

    } else {

         printf("%s Was Not Found\n\n", productName);

    }
 }



int main(){

    inputData();
    inputData();
    inputData();

    outputData();

    searchForProduct("Tomato");

    removeProduct("Egg");
    removeProduct("Tomato");


    return 0;

    free(pFirstNode);
    free(pLastNode);
}
