
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 20




// Write a program that reads in a string with max 20 characters from stdin to a
//     local string variable . Copy the string to another string by using :
// a) The library function strcpy(..)
// b) A function void copyString(...) not using any libray function.
// Main program ends by printing out the string on the std out ( console).
// The program should be able to read in from keyboard or from a textfile ‘myfile.txt’
//      containing one string of characters. (Create this file for test)


// input by keyboard
int main(){
    // read from keyboard
    printf("Enter a stance: \n" );
    char input[MAX];
    fgets(input, MAX, stdin);
    printf("input sentence: %s.\n",input);

    // read from file

    // FILE *fptr;
    // fscanf(fptr,"%[^\n]",input);
    // printf("input from file: %s",input);
    // fclose(fptr);

    // the library function :char *strcpy(char *dest, const char *src)
    // which will copy from src to dest
    char output1[MAX];
    strcpy(output1, input);
    printf("library's output sentence: %s.\n",output1);


    // A function void copyString(...) not using any library function.
    char output2[MAX];
    int i;
    for(i = 0; i < MAX ; i ++){
        output2[i] = input[i];
    }
    printf("copyString's output sentence: %s.\n",output2);
    return (0);



}







// #include <stdlib.h>
// #include <stdio.h>
// #include <string.h>

// void copyString(char* in, char* out);

// int main(int argc, char *argv[])
// {
//     char* s = malloc(sizeof(char) * 25);
//     if(argc > 1)
//     {
//         FILE* fp = fopen(argv[1], "r");
//         fgets(s, 20, fp);
//         printf("Input String: %s", s);
//     }
//     else
//     {
//         printf("Input string: ");
//         fgets(s, 20, stdin);
//     }
//     char* s2 = malloc(sizeof(char) * 25);
//     strcpy(s2, s);
//     char* s3 = malloc(sizeof(char) * 25);
//     copyString(s, s3);
//     printf("strcpy: %scopyString: %s", s2, s3);
//     return 0;
// }

// void copyString(char* in, char* out)
// {
//    for(int i = 0; i < strlen(in); i++)
//    {
//        out[i] = in[i];
//    }
// }
