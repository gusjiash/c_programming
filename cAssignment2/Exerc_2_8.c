/*
 *  File    : Exerc_2_8.c
 *  Program : Nim game
 *  Author  : Shan Jiang
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#define MAX_COINS 13

const int HUMAN = 0;
const int COMPUTER = 1;

int human_choice(int pile);
void write_winner( int player );
int play_again();
int computer_choice(int pile);
int toggle( int player );
/* --------------------- Utilities -------------*/
void clear_stdin();

/***************************************************
 *
 *    MAIN
 *
 ***************************************************/
int main(){

    int pile,         /* This is how many coins we have */
        player,         /* Who is playing? */
        n_coins;            /* Number of coins taken */

    pile = MAX_COINS;     /* Set start values (= init) */
    player = HUMAN;

    srand( time(0) );     /* Setup random */

    printf("V‰lkommen till NIM by ...");


  /*
   *  Program main loop
   */
    bool boolean = true;

    do{
        printf("Det ligger %d  mynt i hˆgen\n", pile );

        if( player == HUMAN ){
            n_coins = human_choice(pile);
        }else{
            n_coins = computer_choice(pile);
            printf("- Computer tog %d\n", n_coins);
        }
        pile -= n_coins;
        player = toggle( player );

        if (pile <= 1){
            write_winner( player );
            clear_stdin();
            boolean = play_again();
                if (boolean == true){
                    pile = MAX_COINS;
                }
        }

    }while (boolean == true);


    printf("Avslutat\n");

}

/******************************************************
 *
 *  DEFINITIONS
 *
 ******************************************************/


void clear_stdin(){
  while( getchar() != '\n' ){;}
}

int human_choice(int pile){
  /*
 * human_choice
 * Get human choce as an int from stdin.
 * Clears stdin.
 * in: pile
 * out: int-value in range 1-3 (and also less than pile)
 */
    int human;

    printf("Your turn: \n");
    scanf(" %d", &human);
    while ((human < 0 || human > 3)|| human > pile){
        printf("Wrong input: %d , Your turn again: \n", human);
        scanf(" %d", &human);
    }
    printf("human_choice: %d\n",human);
    return human;

}

int computer_choice(int pile){
/*
 * computer_choice
 * Get computers choice (including some AI,
 * if 4 coins or less left, function makes a
 * smart choice else random).
 * in: pile
 * out: int-value in range 1-3 (and also less than pile)
 */
    int computer;
    if (pile > 4){
        computer = rand() % 3 + 1;
    }else if(pile > 1){
        computer = pile - 1;
    }else{
        computer = 1;
    }

    return computer;
}

void write_winner(int player ){
/*
 * write_winner
 * Write winner as a string on stdout.
 * in: Values HUMAN or COMPUTER.
 * out:
 */
    if (player == HUMAN){
        printf("You lose\n");
    }else{
        printf("You win\n");
    }
}

int play_again(){
/*
 * play_again
 * Ask human if he/she wants to play
 * another round. If 'n' or 'N' selected
 * (single char) return false else true.
 * Clears stdin.
 * in:
 * out: true or false
 */
    char result;
    printf("Do you want to play again? (Y/N) \n");
    scanf(" %c", &result);
    if( result == 'n' || result == 'N'){
        return false;
    }else{
        return true;
    }

}

int toggle( int player ){
/*
 * toggle
 * Switches player, if HUMAN in COMPUTER out
 * etc.
 * in:  actual player
 * out: next player
 */
    if (player == 1){
        return 0;
    }else{
        return 1;
    }

}

