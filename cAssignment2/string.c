#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void PrintLable(char the_label[]);
void Print_Lable(char *the_label);
void crateSpace(int length);
void changeContent(char the_label[]);

int length = 10;

int main(void){
    char label[] = "Single"; /* 7 character */
    char new_label[10] = "Single"; /* 10 character */
    char label2[10] = "Married";
    char *labelPtrl;
    labelPtrl = label;

    printf("Third char is : %c \n",labelPtrl[2]);

    PrintLable(label);
    Print_Lable(labelPtrl); /*result are the same */
    //When we declare an array as the parameter to a function,
    //we really just get a pointer. Plus, arrays are always
    //automatically passed by reference (e.g., a pointer is passed).

    changeContent(label);
}

void PrintLable(char the_label[]){
    printf("Label: %s\n", the_label);
}
/*  this two function are the same base on the same reason*/
/*  There is no difference because in both cases the parameter is really a pointer.*/

void Print_Lable(char *the_label){
    printf("Label: %s\n", the_label);
}

void crateSpace(int length){
    char *str;
    /*
        Dont forget extra char for NUL character
     */
    str = (char *)malloc(sizeof(char) * (length + 1));
    /*
        Ask malloc()  to give us back enough space for a string of desired size.
        We need the size: size of one char * number of characters with extra 1.
        Keep track of the dynamically-allocate array with a pointer.
     */
}

void changeContent(char the_label[]){
    /*
     *    The only way to change the contents of an array is to make
     *    change to each element in the array.

     *    strlen(str)  -> return number of char in the string.
     *                    not include the NUL.
     *    strcmp(str1,str2)  -> compare two string
     *                            equal -> 0
     *    strcpy(dest, source) -> copy contents from source to dest
     */
    char str[20] = "second";
    strcpy(str,the_label);
    printf("old: second \n new: %s \n", str);
}
















