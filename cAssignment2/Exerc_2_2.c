
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define MAX 20

int main(){

    srand(time(NULL));
    int i;
    int array[MAX];
    int *array_ptr[MAX];


//  Crate int array using int array
    for (int i = 0; i < MAX; ++i){
        int x = rand() % 20;
        array[i] = x;
    }
    for ( i = 0; i < MAX; i++){
        printf("Value of var[%d] = %d\n", i, array[i]);
    }


   //  Address value for array[MAX]
    printf("the address of array[MAX] = %p \n",&array);
   // First integer vale
    printf("array[%d] = %d \n",0, array[0]);
   //  Size of integer
    printf("size of %d is %lu \n",array[0], sizeof(array[0]));
   // Size of array
    printf("size of array is %lu \n",sizeof(array));
   // Double all value
    int *pointer;
   // pointer to array
    pointer = array;
    while(*pointer){
        //this can not use in any other thing instead of array,
        //since it will not goes to the '0'
        *pointer = *pointer * 2;
        pointer ++;
    }

    for ( i = 0; i < MAX; i++){
        printf("Value of var[%d] = %d\n", i, array[i]);
    }

}

