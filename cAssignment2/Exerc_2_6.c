#include <stdio.h>
#include <stdlib.h>
#define MAX 5

// adds a number in the queue according to the rules ,
// and returns number 1 if the number could be entered and 0
// if the queue is full
int input(int list[], int number);
void initQue(int list[]);
int takeout(int list[]);

// initializes the list to initially contain
// five vacant positions (‐1)


int main(void){
    int startList[MAX];
    initQue(startList);
    input(startList,3);
    input(startList,7);
    input(startList,6);
    input(startList,5);
    input(startList,1);
    input(startList,4);
    takeout(startList);

    return(0);
}

void initQue(int list[]){
    for (int i = 0; i < MAX; ++i){
        list[i] = -1;
    }
    printf("\n Queue from the start: ");
    for (int i = 0; i < MAX; ++i){
        printf("%d ",list[i]);
    }
}
int input(int list[], int number){
    int i = 0;
    while (i < MAX-1 && list[i] != -1){
        i++;
    }
    if( i < 5 && list[i] == -1){
        list[i] = number;
    }

    printf("\n Queue after you put in %d: ",number);
    for (int i = 0; i < MAX; ++i){
        printf("%d ",list[i]);
    }
    return *list;
}
int takeout(int list[]){
    for (int i = 0; i < MAX -1 ; ++i){
        list[i] = list[i+1];
    }
    list[MAX - 1] = -1;
    printf("\n Queue after taken out an integer");
    for (int i = 0; i < MAX; ++i){
        printf("%d ",list[i]);
    }
    return *list;
}

