
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 100

// Create a program that reads in two strings from the command line and checks
// if it is two strings and if the two strings are identical or not.
// Do this with and without library function strcmp(..).
    int main(){
        printf("Enter one string \n");
        char string1[MAX];
        fgets(string1, MAX, stdin);
        printf("Enter one more string\n");
        char string2[MAX];
        fgets(string2, MAX, stdin);
    // with strcmp(...)
        if (strcmp(string1,string2) == 0){
            printf("they are same \n");
        }else if (strcmp(string1,string2) != 0){
            printf("they are different \n");
        }
    // without stremp(..)
        char *string1_pointer;
        string1_pointer = string1;
        char *string2_pointer ;
        string2_pointer = string2;

        string1[strlen(string1)-1] = '\0';
        string2[strlen(string2)-1] = '\0';


        while( (*string1_pointer == *string2_pointer) && (*string2_pointer != '\0')){
            string1_pointer++;
            string2_pointer++;
        }
            if (*string1_pointer == *string2_pointer) {
                printf("they are equal \n");
            }else{
                printf("they are not equal\n");;
            }
    }












