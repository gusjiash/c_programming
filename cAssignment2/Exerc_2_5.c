
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define MAX 100
#define MAXNUMBER 20
// ‐‐‐‐‐‐ Function declaration ‐‐‐‐‐‐‐‐‐‐ Updated 2014 after wp2 void create_random( int *tab );

void count_frequency(int *tab, int *freq );
void draw_histogram(int *freq );

int main ( void){
    int table[MAX],rand_value;
    int frequency[MAXNUMBER];


    srand(time(0));
    for (int i = 0; i < MAX; ++i){
        rand_value = rand()%MAXNUMBER;
        table[i] = rand_value;
        printf("random value: %d\n",rand_value );
    }
    for(int i = 0; i < MAXNUMBER; ++i){
        frequency[i] = 0;
    }
    count_frequency(table,frequency);
    draw_histogram(frequency);
    return (0);
}

void count_frequency(int *tab, int *freq){
    for (int i = 0; i < MAX; ++i){
        *(freq + *tab)+=1;
        printf("%d add till %d\n",*tab,*(freq + *tab));
        tab++;
    }
}
void draw_histogram(int *freq ){
    for (int i = 0; i < MAXNUMBER; ++i){
        if (*freq != 0){
            printf("\n%d ",i);
                while(*freq > 0){
                    printf("x");
                    *freq-=1;
                }
        }
        freq++;
    }
}



