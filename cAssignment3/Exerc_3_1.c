#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 50


enum DIRECTION {N,E,S,W};
typedef struct{
    int xpos;
    int ypos;
    enum DIRECTION dir;
}ROBOT;

void move(ROBOT *rob);
void turn(ROBOT *rob);
// int turn(x,y,direct);

int main(void){
    ROBOT rob;
    char currentp;

    char *movement = malloc(sizeof(char) * (MAX-1));

    printf("Robot new start position(num num m/t): \n");
    scanf(" %d %d %s", &(rob.xpos), &(rob.ypos), movement);

    rob.dir = 0;

    while( *movement != '\n' && *movement != EOF){
        if (*movement == 'm'){
            move(&rob);
            movement++;
        }else if(*movement == 't'){
            turn(&rob);
            movement++;
        }else{
            break;
        }
    }
    printf("Last:current position: %d %d \n", rob.xpos, rob.ypos);

    return 0;
}

void move(ROBOT *rob){
    switch(rob->dir){
        case 0:
            rob->ypos++;
            break;
        case 1:
            rob->xpos++;
            break;
        case 2:
            rob->ypos--;
            break;
        case 3:
            rob->xpos--;
            break;
    }
    printf("current position: %d %d \n", rob->xpos, rob->ypos);

}

void turn (ROBOT *rob){
    if ((rob->dir) < 3){
        rob->dir++;
    }else{
        rob->dir = 0;
    }
    printf("current position: %d %d \n", rob->xpos, rob->ypos);
}














