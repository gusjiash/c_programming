

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

//  Konstanter

#define MAX 5

//  Typedefs

typedef struct q{
    int number;
    struct q *next;
    struct q *prev;
} REGTYPE;

REGTYPE* random_list(void);
REGTYPE* add_first(REGTYPE * temp, int data);
REGTYPE* insert(int x, REGTYPE* head);




int main(int argc, char *argv[]){
    int nr=0;
    REGTYPE *active_post,*head = NULL, *second;

    srand(time(0));

    head = add_first(head,2);
    second=random_list();

    head->next = second;
    second->prev = head;

    active_post = head;

    while(active_post != NULL){

        printf("element at: %d , value: %d  \n", nr++, active_post->number );

        active_post = active_post->next;

    }

    while((active_post = head) != NULL){
        head = active_post->next;
        free(active_post);
    }


    return 0;
}

REGTYPE* add_first(REGTYPE * temp, int data){
// first condition: when the list is empty
    REGTYPE* head;
    head = (REGTYPE*)malloc(sizeof(REGTYPE));

    head->number = data;
    head->prev = NULL;
    head->next = temp;
    temp = head;

    return temp;

}





REGTYPE* random_list(void){
    int nr,i=0;
    REGTYPE *top, *old, *item;

    top = (REGTYPE*) malloc(sizeof(REGTYPE));
    nr = rand() %100;

    top->number = nr;
    top->next = NULL;
    top->prev = NULL;

    item = top;
    printf("RANDON_LIST 1: i: %d value: %d  \n", i, top->number );
    i++;

    while(i < MAX){
        old = item;
        item = (REGTYPE*) malloc(sizeof(REGTYPE));

        nr = rand() % 100;

        item->number = nr;
        item->prev = old;
        item->next = NULL;

        old->next = item;

        printf("RANDON_LIST 2: i: %d value: %d\n", i, item->number);

        i++;
    }

    return(top);
}


