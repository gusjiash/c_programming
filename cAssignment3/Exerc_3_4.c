
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

typedef struct{
    char firstname[20];
    char lastname[20];
    char pers_number[13]; //yyyy-mm-dd-xxxx
}PERSON;

#define FILENAME personInformation.dat

//Function declaration

//Reads in a person record;
PERSON input_record(void);
//Creat a file and write a first input_record
void write_new_file(PERSON* inrecord);
//Print out all persons in the file
void printfile(void);
//Print out person if in list
void search_by_firstname(char* name);
//Appends a new person to file;
void append_file(PERSON* inrecord);
//Print all persons in alphabetical order
void printfileAlphabetic(void);
//sort function
void sort (PERSON* sortArray,int size);
n
int main(void){
    bool done = false;
    PERSON ppost,temp;
    char searchName[20];
    char* searchPointer;
    int choice;

    while(!done){

        printf("Choice operation: 1(create) 2(add) 3(search) 4(printAll) 5(printAlpha) 6(done)\n");
        scanf(" %i", &choice);
        while(getchar() != '\n');
        switch(choice){
            case 1:
                ppost = input_record();
                write_new_file(&ppost);
                break;
            case 2:
                temp = input_record();
                append_file(&temp);
                break;
            case 3:
                printf("Enter search name:\n");
                scanf(" %s",searchName);
                searchPointer = searchName;
                break;
            case 4:
                printfile();
                break;
            case 5:
                printfileAlphabetic();
                break;
            case 6:
                done = true;
                break;
            default:
                break;
            }
    }

    return(0);
}




//Reads in a person record;
PERSON input_record(void){

    PERSON inrecord;
    char temp[20];



    printf("Enter first name: \n");
    scanf(" %s",temp);
    strcpy(inrecord.firstname,temp);

    printf("Enter last name: \n");
    scanf(" %s",temp);
    strcpy(inrecord.lastname,temp);

    printf("Enter personal number: \n");
    scanf(" %s",temp);
    strcpy(inrecord.pers_number,temp);

    return(inrecord);
}

//Creats a file and write a first record
void write_new_file(PERSON* inrecord){

    FILE* iFile;
    iFile = fopen("FILENAME","w");

    fprintf(iFile, "%s %s %s\n",
            inrecord->firstname,inrecord->lastname,inrecord->pers_number);

    fclose(iFile);
}



//Print out all persons in the file
void printfile(void){

    FILE* oFile;
    PERSON *outRecord;
    int i = 1;

    outRecord = (PERSON*)malloc(sizeof(PERSON));

    oFile = fopen("FILENAME","r");

    if (oFile == NULL){
        printf("File not exist\n");
    }
    printf("Record from file\n");
    while(fscanf(oFile, "%s %s %s\n",
        outRecord->firstname,outRecord->lastname,outRecord->pers_number) == 3){

        printf("line: %d First name: %s \t lastname name: %s \t personalnumber: %s\n",
            i,
            outRecord->firstname,
            outRecord->lastname,
            outRecord->pers_number);

        printf("test: %d\n",outRecord->firstname[0]);
        i++;
    }

    fclose(oFile);


}
//Print out person if in list
void search_by_firstname(char* name){

    FILE* oFile;
    PERSON *outRecord;
    int i;
    bool isFound;
    char choice[1];

    outRecord = (PERSON*)malloc(sizeof(PERSON));

    oFile = fopen("FILENAME","r");

    isFound = 0;

    printf("Search for First name or Last name (f/l)\n");
    scanf(" %c", choice);

    if (!(choice[0] == 'f' || choice[0] == 'l')){
        printf("Invalid search\n");
    }

        while(fscanf(oFile, "%s %s %s\n",
            outRecord->firstname,outRecord->lastname,outRecord->pers_number) == 3){

            if ((choice[0] == 'f' && !strcmp(name,outRecord->firstname))||
                (choice[0] == 'l' && !strcmp(name,outRecord->lastname))){
                printf("line: %d First name: %s \t lastname name: %s \t personalnumber: %s\n",
                i,
                outRecord->firstname,
                outRecord->lastname,
                outRecord->pers_number);
                isFound = 1;
            }
        }

        if (isFound == 0) {
            printf("Cant find in the file\n");
        }



    fclose(oFile);

}
//Appends a new person to file;
void append_file(PERSON* inrecord){

    FILE* iFile;
    iFile = fopen("FILENAME","a");

    if(iFile == NULL){
        printf("This file does not exist\n");
    }

    fprintf(iFile, "%s %s %s\n",
            inrecord->firstname,inrecord->lastname,inrecord->pers_number);

    fclose(iFile);

}

void printfileAlphabetic(void){

    FILE* oFile;
    PERSON* outRecord;
    PERSON* fnameList;
    int n = 0,i,size = 10;

    fnameList = malloc(sizeof(PERSON) * size);

    oFile = fopen("FILENAME","r");

    printf("come to here \n");

    while(fscanf(oFile," %s %s %s\n",
        outRecord->firstname, outRecord->lastname, outRecord->pers_number) == 3){

        *(fnameList + n) = *outRecord;

        n++;
    }
    // sort(fnameList,size);

    fclose(oFile);

    sort(fnameList,n);

}


void sort (PERSON* sortArray,int size){

    int i,j;
    PERSON temp;

    for (i = 0; i < (size-1) ; ++i){
        for (j = 0; j < (size - i - 1); ++j){
            if ( strcmp(sortArray[j].lastname,sortArray[j+1].lastname) > 0){
                temp = sortArray[j];
                sortArray[j] = sortArray[j+1];
                sortArray[j+1] = temp;
            }
        // printf("J index: %d  value: %s\n", j, sortArray[j].lastname);
        }
    }

    printf("Sorted result\n");

    for (int i = 0; i < size; ++i){
        printf("line:%d %s %s %s \n",i,sortArray[i].firstname,sortArray[i].lastname,sortArray[i].pers_number);
    }

}










