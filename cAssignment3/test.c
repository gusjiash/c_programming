#include <stdio.h>
#include <stdlib.h>

enum DIRECTION{N,O,S,W};

typedef struct {
    int xpos;
    int ypos;
    enum DIRECTION dir;
}ROBOT;

void move();
void turn();

int main(){
    ROBOT roro;
    char temp;
    puts("please input the robot's initial position and direction");
    scanf("%d %d %c", &(roro.xpos), &(roro.ypos), &temp);
    if(temp == 'N'){
        roro.dir = N;
    }else if (temp == 'S'){
        roro.dir = S;
    }else if (temp == 'O'){
        roro.dir = O;
    }else if (temp == 'W'){
        roro.dir = W;
    }else{
        puts("wrong direction!!");
    }
    //roro.dir = temp;
    //fflush(stdin);
    fgetc(stdin);
    puts("please input the movement of the robot ('m' for move and 't' for turn)");

    while(stdin){
        int tmpt = fgetc(stdin);
        printf("%c", tmpt);
        if(tmpt== 'm'){
            move(&roro);
        }else if(tmpt =='t'){
            turn();
        }else if (tmpt == '\n'){
            break;
        }else{
            puts("invalid\n");
        }
    }

    printf("%d, %d, %d \n", roro.xpos, roro.ypos, roro.dir);
}

void move(ROBOT * ro ){
    puts("move\n");
    //printf("%c %d\n", ro->dir, ro->dir);
    switch (ro -> dir) {
        case N :
            ro ->ypos --;
            break;
        case S :
            ro ->ypos ++;
            break;
        case O :
            ro ->xpos ++;
            break;
        case W :
            ro ->xpos --;
            break;
        default:
            puts("wrong direction");
        }
    printf("%d, %d, %d \n", ro->xpos, ro->ypos, ro->dir);
}

void turn(){
    puts("turn\n");
}











#include <stdio.h>
#include <stdlib.h>


int search_number(int number, int tab[], int size);

void sort ( int tab [],int number);

int main(void){
    int x;
    int test[] = {4,2,7,10,3};
    int size;
    int searchResult;
    int mini;

    printf("enter number:\n");
    scanf(" %d", &x);

    size = sizeof(test)/sizeof(test[0]);

    searchResult = search_number(x,test,size);
    printf("searchResult:%d\n",searchResult);

    sort(test,size);
    printf("sorted result: \n");
    for (int i = 0; i < size; ++i){
        printf("%d ",test[i]);
    }

}

int search_number(int number, int tab[], int size){
    for (int i = 0; i < size; ++i){
        if(tab[i] == number){
            return i+1;
        }
    }
    return -1;

}

void sort ( int tab [],int size){
    int temp;
    for (int i = 0; i < (size - 1) ; ++i){
        for (int j = 0; j < (size - 1) - i; ++j){
            if (tab[i] > tab[i + 1]){
                temp = tab[i];
                tab[i] = tab[i + 1];
                tab[i + 1] = temp;
            }
        }
    }
}





















