#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

unsigned char checkInput(unsigned char input[3]);
unsigned int convertToHex(unsigned char input[2]);
void hexToBinary(unsigned int hexValue);
void printTable(char engine, char gear, char key, char brake1, char brake2);
void printBinary(unsigned integer);


char engine, gear, key, brake1, brake2;

int main(void){
    unsigned char input[3];
    int x = 0;
    unsigned char temp;
    unsigned int hexValue;

    unsigned number;


    printf("Enter a two char hexadecimal form integer: \n");

    scanf(" %s", input);

    if(checkInput(input)){

        hexValue = convertToHex(input);

        hexToBinary(hexValue);
        printTable(engine, gear, key, brake1, brake2);

    }
}


unsigned char checkInput(unsigned char input[3]){

    int x = 0;


    while(x<2){
        if (input[x] == ' '){
            printf("missing input\n");
            return 0;
        }
        if (input[x] >= 65){
            input[x] = toupper(input[x]);
        }
        if (!( (input[x] > 47 && input[x] < 58) ||
               ((input[x] > 64 && input[x] < 71) || (input[x] > 96 && input[x] < 103)) )){
            printf("invalid input \n");
            return 0;
        }
        x++;
    }

    if (input[2] == ' ' || input[2] > 48){
        printf("error message: enterd char is too longs: %c\n", input[2]);
        return 0;
    }

    return *input;

}



unsigned int convertToHex(unsigned char input[2]){

    unsigned int hexValue;
    int temp[2];
    int x = 0;

    while( x < 2){
        if (input[x] > 57){
            temp[x] = input[x]  - 55;
        }else{
            temp[x] = input[x] - '0';
        }
        x++;
    }

    hexValue = temp[0] * 16 + temp[1];
    return hexValue;

}

void hexToBinary(unsigned int hexValue){

    engine = (hexValue & 128) >> 7;
    gear = (hexValue & 112) >> 4;
    key = (hexValue & 12) >> 2;
    brake1 = (hexValue & 2) >> 1;
    brake2 = (hexValue & 1);

}

void printTable(char engine, char gear, char key, char brake1, char brake2){

    printf("Name         Value\n");
    printf("------------------\n");
    printf("engine_on       %d\n",engine);
    printf("gear_pos        %d\n",gear);
    printf("key_pos         %d\n",key);
    printf("brake1          %d\n",brake1);
    printf("brake2          %d\n",brake2);

}


void printBinary(unsigned integer){

    unsigned i;
    unsigned one = 1 << 7;



    printf("%d 's binary form: ", integer);
    for (i = 1; i <= 8 ; ++i){
        putchar( integer & one ? '1' : '0');
        integer <<= 1;
        if (i % 4 == 0){
            putchar(' ');
        }
    }

}
// AB
// 10 * 16  + 11  = 171
