#include <stdio.h>

void packCharacters( char c1, char c2 );
void displayBits( unsigned value );
void unpack( unsigned value );

int main()
{
   unsigned num;
   char char1, char2;

   printf( "Enter 2 characters: " );
   scanf( "%c %c", &char1, &char2 );
   printf( "Character 1 in binary: \n" );
   displayBits( char1 );
   printf( "Character 2 in binary: \n" );
   displayBits( char2 );

   printf( "Packed characters in binary: \n" );
   packCharacters( char1, char2 );
   printf( "\n" );
   // system( "PAUSE" );
   return 0;
}

void displayBits( unsigned value ){
   unsigned c, displayMask = 1 << 31;
   printf( "%7u = ", value );
   for ( c = 1; c <= 32; c++ ) {
      putchar( value & displayMask ? '1' : '0' );
      value <<= 1;
      if ( c % 8 == 0 )
         putchar( ' ' );
   }
   putchar( '\n' );
}
void packCharacters( char c1, char c2 )
{
   unsigned packed;
   packed = c1;
   packed <<= 8;
   packed |= c2;
   printf( "CHARACTER - %c\n", packed );
   displayBits( packed );
   printf( "\n" );
   unpack( packed );
}
void unpack( unsigned value ){
   displayBits(value);
   char c1, c2;
   c1 = value;
   printf( "Character 2 unpacked: %d\n", c1 );
   value >>= 8;
   displayBits(value);
   c2 = value;
   printf( "CHaracter 1 unpacked: %d\n\n", c2 );
}



