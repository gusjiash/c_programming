#include <stdio.h>
#include <stdlib.h>

void printTable(char engine, char gear, char key, char brake1, char brake2);
int storeInBit(char engine, char gear, char key, char brake1, char brake2);
void displayBits(unsigned value );
void printBinary(unsigned num);



int main(void){

    char engine, gear, key, brake1,brake2;

    unsigned int u = 0 ;



    printf("Enter 5 number: engine(0..1),gear(0..4),key(0..2),brake1(0..1),brake2(0..1)\n");
    engine = getchar() - 48;
    if (engine == ' ' || (engine < 0 || engine > 1)){
        printf("Error message: Wrong engine number\n");
        return 1;
    }
    getchar();

    gear = getchar() - 48;
    if (gear == ' ' || (gear < 0 || gear > 4)){
    printf("Error message: Wrong gear number\n");
    return 1;
    }
    getchar();

    key = getchar() - 48;
    if (key == ' ' || (key < 0 || key > 2)){
    printf("Error message: Wrong key number\n");
    return 1;
    }
    getchar();

    brake1 = getchar() - 48;
    if (brake1 == ' ' || (brake1 < 0 || brake1 > 1)){
    printf("Error message: Wrong brake1 number\n");
    return 1;
    }
    getchar();

    brake2 = getchar() - 48;
    if (brake2 == ' ' || (brake2 < 0 || brake2 > 1)){
    printf("Error message: Wrong brake2 number\n");
    return 1;
    }
    getchar();


    printTable(engine, gear, key, brake1, brake2);

    u = storeInBit(engine, gear, key, brake1, brake2);

}

void printTable(char engine, char gear, char key, char brake1, char brake2){

    printf("Name         Value\n");
    printf("------------------\n");
    printf("engine_on       %d\n",engine);
    printf("gear_pos        %d\n",gear);
    printf("key_pos         %d\n",key);
    printf("brake1          %d\n",brake1);
    printf("brake2          %d\n",brake2);

}

int storeInBit(char engine, char gear, char key, char brake1, char brake2){

    unsigned int u = 0 ;

    u <<= 1;
    u |= engine;

    u <<= 3;
    u |= gear;

    u <<= 2;
    u |= key;

    u <<= 1;
    u |= brake1;

    u <<= 1;
    u |= brake2;

    printf("hexadeciaml form:%X\n", u);
    return u;
}

void displayBits(unsigned value ){

   unsigned c, displayMask = 1 << 7;
   printf( "%7u =  \n", value );
   for ( c = 1; c <= 8; c++ ) {
      putchar( value & displayMask ? '1' : '0' );
      value <<= 1;
      if ( c % 4 == 0 )
         putchar( ' ' );
   }
   putchar( '\n' );
}

void printBinary(unsigned num) {

    int index = 0;
    printf("Dec:%d As binary = ",num);
    for(index = 7; index >= 0; index--) {
        if( (1 << index) & num) {
            printf("1");
        } else  {
            printf("0");
        }
        if(index!=7 && index%4==0) {
            printf(" ");
        }
    }
    printf("\n");
}

